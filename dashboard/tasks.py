from celery import shared_task
import pandas as pd
from pandas.io.json import json_normalize
from sklearn.model_selection import train_test_split

from core.models import Recommendation
from moviesapp.models import Movie, Genre
from ratingsapp.models import Rating
from usersapp.models import BasicUser
import pandas as pd
import numpy as np
import turicreate as tc
import requests
from sklearn.metrics.pairwise import pairwise_distances


def get_the_users_dataframe():
    '''
    It generates the User dataframe
    '''
    user_values = BasicUser.objects.values('age', 'id', 'occupation', 'sex', 'zip_code')
    users = pd.DataFrame.from_records(user_values)
    users.columns = ['age', 'user_id', 'occupation', 'sex', 'zip_code']
    return users


def get_the_ratings_dataframe():
    '''
    It generates the ratings dataframe
    '''
    rating_values = Rating.objects.values('created_at', 'movie', 'score', 'user_id')
    ratings = pd.DataFrame.from_records(rating_values)
    ratings.columns = ['unix_timestamp', 'movie_id', 'rating', 'user_id']
    return ratings


def get_the_ratings_dataframe_by_consuming_API(api_token, url):
    '''
    It generates the ratings dataframe [by consuming the API] ~18 seconds
    '''
    headers = {'Authorization': 'Token ' + api_token}
    ratings_response = requests.get(url, headers=headers)
    ratings_json = ratings_response.json()
    ratings = json_normalize(ratings_json)


def get_the_movies_dataframe():
    '''
    It generates the movies dataframe with th genres relation
    '''
    list_of_genre_ids = Genre.objects.values_list('id', flat=True)
    list_of_movie_ids = Movie.objects.values_list('id', flat=True)

    genres_count = {}
    for genre_id in list_of_genre_ids:
        genres_count[str(genre_id)] = 0

    movie_values = []
    for id in list_of_movie_ids:
        movie = Movie.objects.get(id=id)
        movie_genres = movie.genres.values_list('id', flat=True)

        movie_dict = {
            'movie_id': id,
            'movie_title': movie.title,
            'release_date': movie.release_date,
            'video_release_date': movie.video_release_date,
            'imdb_url': movie.imdb_url
        }
        z = {**movie_dict, **genres_count}

        for genre_id in list_of_genre_ids:
            if genre_id in movie_genres:
                genres_count[str(genre_id)] = 1
            else:
                genres_count[str(genre_id)] = 0

        movie_values.append(z)

    return pd.DataFrame.from_records(movie_values)


def predict(ratings, similarity, type='user'):
    '''
    This gives us the item-item and user-user similarity in an array form
    '''
    if type == 'user':
        mean_user_rating = ratings.mean(axis=1)
        ratings_diff = (ratings - mean_user_rating[:, np.newaxis])
        pred = mean_user_rating[:, np.newaxis] + similarity.dot(ratings_diff) / np.array(
            [np.abs(similarity).sum(axis=1)]).T
    elif type == 'item':
        pred = ratings.dot(similarity) / np.array([np.abs(similarity).sum(axis=1)])
    return pred


def generate_TOP_N_by_popularity_model(n, train_data, user_id):
    '''
    This gives us the recommendations by most popular choices
    '''
    popularity_model = tc.popularity_recommender.create(train_data,
                                                        user_id='user_id',
                                                        item_id='movie_id',
                                                        target='rating')
    # Predict the top five for the single user
    popularity_recomm = popularity_model.recommend(users=[user_id], k=n)
    popularity_recomm.print_rows(num_rows=25)
    top_n = popularity_recomm.to_dataframe()
    return top_n


def generate_TOP_N_by_similarity_model(n, train_data, user_id):
    '''
    This gives us the recommendations by most popular choices
    '''
    # Training the model
    item_sim_model = tc.item_similarity_recommender.create(train_data,
                                                           user_id='user_id',
                                                           item_id='movie_id',
                                                           target='rating',
                                                           similarity_type='cosine')
    # Making recommendations
    item_sim_recomm = item_sim_model.recommend(users=[user_id], k=n)
    item_sim_recomm.print_rows(num_rows=25)
    top_n = item_sim_recomm.to_dataframe()
    return top_n


def update_recommedations_to_the_user(user, top):
    '''
    It updates the recommendation MOdel based on the top n recommendations engine results
    '''

    # Remove the previous recommendations
    Recommendation.objects.filter(user=user).delete()

    for _, row in top.iterrows():
        movie_id = int(row['movie_id'])
        score = float(row['score'])
        rank = int(row['rank'])
        movie = Movie.objects.filter(id=movie_id)

        if movie.count() > 0:
            Recommendation.objects.create(movie=movie.first(),
                                          score=score,
                                          rank=rank,
                                          user=user)
        else:
            print("MOVIE ID: " + str(movie_id) + " WAS NOT FOUND IN DATABASE")



# Test with files
def _data_file_results(user_id):
    # Reading users file:
    u_cols = ['user_id', 'age', 'sex', 'occupation', 'zip_code']
    users = pd.read_csv('data/ml-100k/u.user', sep='|', names=u_cols, encoding='latin-1')

    # Reading ratings file:
    r_cols = ['user_id', 'movie_id', 'rating', 'unix_timestamp']
    ratings = pd.read_csv('data/ml-100k/u.data', sep='\t', names=r_cols, encoding='latin-1')

    # Reading items file:
    i_cols = ['movie id', 'movie title', 'release date', 'video release date', 'IMDb URL', 'unknown', 'Action',
              'Adventure',
              'Animation', 'Children\'s', 'Comedy', 'Crime', 'Documentary', 'Drama', 'Fantasy',
              'Film-Noir', 'Horror', 'Musical', 'Mystery', 'Romance', 'Sci-Fi', 'Thriller', 'War', 'Western']
    items = pd.read_csv('data/ml-100k/u.item', sep='|', names=i_cols,
                        encoding='latin-1')

    r_cols = ['user_id', 'movie_id', 'rating', 'unix_timestamp']
    ratings_train = pd.read_csv('data/ml-100k/ua.base', sep='\t', names=r_cols, encoding='latin-1')
    ratings_test = pd.read_csv('data/ml-100k/ua.test', sep='\t', names=r_cols, encoding='latin-1')

    n_users = ratings.user_id.unique().shape[0]
    n_items = ratings.movie_id.unique().shape[0]

    data_matrix = np.zeros((n_users, n_items))
    for line in ratings.itertuples():
        data_matrix[line[1] - 1, line[2] - 1] = line[3]

    user_similarity = pairwise_distances(data_matrix, metric='cosine')
    item_similarity = pairwise_distances(data_matrix.T, metric='cosine')

    user_prediction = predict(data_matrix, user_similarity, type='user')
    item_prediction = predict(data_matrix, item_similarity, type='item')

    train_data = tc.SFrame(ratings_train)
    test_data = tc.SFrame(ratings_test)

    popularity_model = tc.popularity_recommender.create(train_data, user_id='user_id', item_id='movie_id',
                                                        target='rating')

    popularity_recomm = popularity_model.recommend(users=[user_id], k=5)
    #popularity_recomm.print_rows(num_rows=25)

    df = popularity_recomm.to_dataframe()
    popularity_model = df.to_dict()

    # Training the model
    item_sim_model = tc.item_similarity_recommender.create(train_data, user_id='user_id', item_id='movie_id',
                                                           target='rating', similarity_type='cosine')

    # Making recommendations
    item_sim_recomm = item_sim_model.recommend(users=[user_id], k=5)
    item_sim_recomm.print_rows(num_rows=25)

    #df2 = item_sim_recomm.to_dataframe()
    #similarity_model = df2.to_dict()



@shared_task
def similarity_ENGINE_JOB(user_id):
    '''
    It is a task responsible for process the collaborative similarity matrix
    '''

    # Current User
    current_user = BasicUser.objects.get(id=user_id)

    # Get Users
    users = get_the_users_dataframe()

    # Get Users
    ratings = get_the_ratings_dataframe()

    # Divide Current ratings list into train and test
    ratings_train, ratings_test = train_test_split(ratings, test_size=0.0943)

    # Calculate the number of unique users and movies
    # n_users = ratings.user_id.unique().shape[0]
    # n_items = ratings.movie_id.unique().shape[0]

    last_user_cnt = 0
    if BasicUser.objects.count() > 0:
        last_user = BasicUser.objects.last()
        last_user_cnt = last_user.id

    last_movie_cnt = 0
    if BasicUser.objects.count() > 0:
        last_movie = Movie.objects.last()
        last_movie_cnt = last_movie.id

    # GEt the user-item matrix
    data_matrix = np.zeros((last_user_cnt, last_movie_cnt))
    for line in ratings.itertuples():
        # line[4] -  USER ID
        # line[2] -  MOVIE_ID
        # line[3] -  SCORE
        data_matrix[line[4] - 1, line[2] - 1] = line[3]

    # Calculate the cosine similarity
    user_similarity = pairwise_distances(data_matrix, metric='cosine')
    item_similarity = pairwise_distances(data_matrix.T, metric='cosine')

    # Predictions
    user_prediction = predict(data_matrix, user_similarity, type='user')
    item_prediction = predict(data_matrix, item_similarity, type='item')

    # Convert the dataset in SFrames
    train_data = tc.SFrame(ratings_train)
    test_data = tc.SFrame(ratings_test)

    # Get The TOP 5 Recommendations for the current user id
    user_id = current_user.order
    top_five = generate_TOP_N_by_similarity_model(5, train_data, user_id)

    # Update the recommendations to the user
    update_recommedations_to_the_user(current_user, top_five)

    # _data_file_results(user_id)