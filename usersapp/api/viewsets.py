from rest_framework import generics
from rest_framework.authentication import TokenAuthentication, SessionAuthentication
from rest_framework.permissions import IsAuthenticated
from rest_framework.viewsets import ModelViewSet
from usersapp.api.serializers import OccupationSerializer, BasicUserSerializer
from usersapp.models import Occupation, BasicUser


class OccupationsViewSet(ModelViewSet):
    serializer_class = OccupationSerializer
    permission_classes = (IsAuthenticated,)
    authentication_classes = (TokenAuthentication, SessionAuthentication,)

    def get_queryset(self):
        return Occupation.objects.all()

class BasicUsersViewSet(ModelViewSet):
    serializer_class = BasicUserSerializer
    permission_classes = (IsAuthenticated,)
    authentication_classes = (TokenAuthentication, SessionAuthentication,)

    # GEt the user list ordered by created_at desc
    def get_queryset(self):
        return BasicUser.objects.order_by('-created_at').all()

class BsicUserList(generics.ListCreateAPIView):
    queryset = BasicUser.objects.all()
    serializer_class = BasicUserSerializer
    permission_classes = (IsAuthenticated,)
    authentication_classes = (TokenAuthentication, SessionAuthentication,)