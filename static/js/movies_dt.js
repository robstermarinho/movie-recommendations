(function (window, $) {
    window.DjangoDataTables = window.DjangoDataTables || {};
    window.DjangoDataTables["datatable"] = $("#MainDatatable").DataTable({
        "serverSide": true,
        "processing": true,
        "ajax": {
            "url": window.movies_dt,
            "method": "GET",
            "async": false,
        },
        "order": [[1, "desc"]],
        "columns": [
            {
                "name": "title",
                "data": 0,
                "title": "<i class='fas fa-film'><\/i> Movie Title",
                "orderable": true,
                "searchable": true
            },
            {
                "name": "release_date",
                "data": 1,
                "title": "Release Date",
                "orderable": true,
                "searchable": true
            },
            {
                "name": "imdb_url",
                "data": 2,
                "orderable": false,
                "searchable": false,
                "title": "IMDb URl"
            },
            {
                "name": "genres",
                "data": 3,
                "title": "Genres",
                "orderable": false,
                "searchable": false
            },
            {
                "name": "ratings",
                "data": 4,
                "orderable": false,
                "searchable": false,
                "title": "<i class='fas fa-star'><\/i> Number of Ratings"
            },

        ],
    });

})(window, jQuery);

jQuery(document).ready(function () {


    //$(document).on('click', '.js-remove-item', HelperComponents.removeItemAsk);
    $(document).on('click', '#add_new', HelperComponents.loadForm);
    $(document).on('submit', '#js-movie-form', HelperComponents.saveForm);
});
