from rest_framework.authentication import TokenAuthentication, SessionAuthentication
from rest_framework.permissions import IsAuthenticated
from rest_framework.viewsets import ModelViewSet
from moviesapp.api.serializers import GenreSerializer, MovieSerializer
from moviesapp.models import Genre, Movie


class GenresViewSet(ModelViewSet):
    serializer_class = GenreSerializer
    permission_classes = (IsAuthenticated,)
    authentication_classes = (TokenAuthentication, SessionAuthentication,)

    def get_queryset(self):
        return Genre.objects.all()


class MoviesViewSet(ModelViewSet):
    serializer_class = MovieSerializer
    permission_classes = (IsAuthenticated,)
    authentication_classes = (TokenAuthentication, SessionAuthentication,)

    def get_queryset(self):
        return Movie.objects.all()
