from django.urls import path
from dashboard import views as dashboard_views

app_name = "dashboard"

urlpatterns = [

    path('', dashboard_views.DashboardHome.as_view(), name='dashboard_home'),

    path('session_lost_check', dashboard_views.SessionLostCheck.as_view(), name='session_lost_check'),

    # BASIC USERS
    path('datatable/basic_users', dashboard_views.BasicUsersDatatable.as_view(), name='basic_users_dt'),
    path('basic_user/create', dashboard_views.BasicUserFormView.as_view(), name='basic_user_create'),

    # RATE MOVIES
    path('datatable/user/<pk>/ratings', dashboard_views.UserRatingsDatatable.as_view(), name='user_ratings_dt'),
    path('user/<pk>/ratings', dashboard_views.UserRatingsView.as_view(), name='user_ratings'),
    path('user/<pk>/top5', dashboard_views.UserTop5MoviesView.as_view(), name='user_top5'),


    path('datatable/movies', dashboard_views.MoviesDatatable.as_view(), name='movies_dt'),
    path('movies', dashboard_views.MoviesView.as_view(), name='movies'),
    path('movies/create', dashboard_views.MoviesFormView.as_view(), name='movie_create'),

]
