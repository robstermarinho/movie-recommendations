from django.test import TestCase
from model_mommy import mommy
from core.models import Recommendation
from moviesapp.models import Movie


# Recommendation Model Test
from usersapp.models import BasicUser


class TestRecommendation(TestCase):

    def setUp(self):
        self.movie = mommy.make(Movie,
                                title='Toy Story (1995)',
                                release_date='1995-01-01',
                                imdb_url='http://us.imdb.com/M/title-exact?Toy%20Story%20(1995)')

        self.user = mommy.make(BasicUser,
                               age=24,
                               sex='M',
                               zip_code='85711')


        self.score = 0.98457283746594837263
        self.rank = 1

        self.recommendation = mommy.make(Recommendation,
                                         user=self.user,
                                         movie=self.movie,
                                         score=self.score,
                                         rank=self.rank)


    def test_recommendation_creation(self):
        self.assertTrue(isinstance(self.recommendation, Recommendation))
        self.assertEqual(self.recommendation.score, 0.98457283746594837263)
        self.assertEqual(self.recommendation.rank, 1)

