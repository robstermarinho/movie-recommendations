var HelperComponents = function () {


    var showAlertError = function (response) {
        console.log(response.responseJSON.detail);

        var detail = 'Request Fail';
        if (typeof response.responseJSON != "undefined") {
            detail = response.responseJSON.detail
        }

        toastr.error(detail, response.status);
        toastr.options = {
            "closeButton": true,
            "debug": false,
            "progressBar": false,
            "positionClass": "toast-top-right",
            "onclick": null,
            "showDuration": "300",
            "hideDuration": "1000",
            "timeOut": "5000",
            "extendedTimeOut": "1000",
            "showEasing": "swing",
            "hideEasing": "linear",
            "showMethod": "fadeIn",
            "hideMethod": "fadeOut"
        };
    };

    var showAjaxError = function (data, modal) {
        var text = JSON.stringify(data.responseText).substr(0, 400) + '...';
        var title = "Request failed";

        if (typeof data.responseJSON != 'undefined')
            text = data.responseJSON.msg


        if (typeof modal != 'undefined')
            modal.modal("hide");

        toastr.error(text, title);
        toastr.options = {
            "closeButton": true,
            "debug": false,
            "progressBar": false,
            "positionClass": "toast-top-right",
            "onclick": null,
            "showDuration": "300",
            "hideDuration": "1000",
            "timeOut": "5000",
            "extendedTimeOut": "1000",
            "showEasing": "swing",
            "hideEasing": "linear",
            "showMethod": "fadeIn",
            "hideMethod": "fadeOut"
        };
    };

    var updateDatatable = function () {
        // Update the Datatable
        if (typeof window.DjangoDataTables != "undefined") {
            var datatable = window.DjangoDataTables["datatable"];
            if (typeof datatable != "undefined" && datatable != null)
                datatable.draw(false);
        }
    };
    // Remove Item using the API endpoint
    var API_DELETE = function (url) {
        jQuery.ajax({
            url: url,
            type: 'DELETE',
            dataType: "json",
            contentType: "application/json; charset=utf-8",
            beforeSend: function (xhr) {
                xhr.setRequestHeader("Authorization", "Token " + window.api_token);
            },
            success: function (data, content, response) {
                toastr.success("Successfully removed.", response.status);
                toastr.options = {
                    "closeButton": true,
                    "debug": false,
                    "progressBar": false,
                    "positionClass": "toast-top-right",
                    "onclick": null,
                    "showDuration": "300",
                    "hideDuration": "1000",
                    "timeOut": "5000",
                    "extendedTimeOut": "1000",
                    "showEasing": "swing",
                    "hideEasing": "linear",
                    "showMethod": "fadeIn",
                    "hideMethod": "fadeOut"
                };

                updateDatatable();
                return false;

            }, error: function (response, abc) {
                showAlertError(response, abc);
            },
        });

    };

    var API_PATCH = function (url, data) {
        jQuery.ajax({
            url: url,
            type: 'PATCH',
            dataType: "json",
            data: JSON.stringify(data),
            contentType: "application/json; charset=utf-8",
            beforeSend: function (xhr) {
                xhr.setRequestHeader("Authorization", "Token " + window.api_token);
            },
            success: function (data, content, response) {
                toastr.success("All changes applied.", response.status);
                toastr.options = {
                    "closeButton": true,
                    "debug": false,
                    "progressBar": false,
                    "positionClass": "toast-top-right",
                    "onclick": null,
                    "showDuration": "300",
                    "hideDuration": "1000",
                    "timeOut": "5000",
                    "extendedTimeOut": "1000",
                    "showEasing": "swing",
                    "hideEasing": "linear",
                    "showMethod": "fadeIn",
                    "hideMethod": "fadeOut"
                };

                updateDatatable();
                return false;

            }, error: function (response, abc) {
                showAlertError(response, abc);
            },
        });

    };

    var API_POST = function (url, data) {
        jQuery.ajax({
            url: url,
            type: 'POST',
            dataType: "json",
            data: JSON.stringify(data),
            contentType: "application/json; charset=utf-8",
            beforeSend: function (xhr) {
                xhr.setRequestHeader("Authorization", "Token " + window.api_token);
            },
            success: function (data, content, response) {
                toastr.success("All changes saved.", response.status);
                toastr.options = {
                    "closeButton": true,
                    "debug": false,
                    "progressBar": false,
                    "positionClass": "toast-top-right",
                    "onclick": null,
                    "showDuration": "300",
                    "hideDuration": "1000",
                    "timeOut": "5000",
                    "extendedTimeOut": "1000",
                    "showEasing": "swing",
                    "hideEasing": "linear",
                    "showMethod": "fadeIn",
                    "hideMethod": "fadeOut"
                };

                updateDatatable();
                return false;

            }, error: function (response, abc) {
                showAlertError(response, abc);
            },
        });

    };


    // Remove ASK
    var removeItemAsk = function (e) {
        e.preventDefault();

        var btn = $(this);
        var url = $(this).data("url");
        var user_order = $(this).data("user_order");

        var title = 'USER_' + user_order;
        swal({
            title: 'Remove ' + title,
            text: "Do you really want to remove the " + title + "?",
            type: "info",
            allowOutsideClick: true,
            showConfirmButton: true,
            showCancelButton: true,
            confirmButtonClass: "btn-danger",
            cancelButtonClass: "btn-success",
            closeOnConfirm: true,
            closeOnCancel: true,
            confirmButtonText: "Remove",
            cancelButtonText: "Cancel",
        }, function (isConfirm) {
            if (isConfirm) {
                API_DELETE(url);
            }
        });

    };

    var loadForm = function (e) {
        e.preventDefault();
        var btn = $(this);
        var modal = $(btn.attr("data-modal"));

        $.ajax({
            url: btn.attr("data-url"),
            type: 'GET',
            dataType: 'json',
            beforeSend: function () {
                App.blockUI({
                    target: btn.closest('.portlet'),
                    animate: true,
                    overlayColor: 'none'
                });
            },
            complete: function () {
                App.unblockUI(btn.closest('.portlet'));
            },

            success: function (data) {
                modal.find('.modal-content').html(data.html_form);
                modal.modal("show");
            },
            error: function (data) {
                showAjaxError(data, modal);
            },
        });
    };


    var AJAXSaveModal = function (url, data, method, modal, datatable, success_action, error_action) {
        $.ajax({
            url: url,
            data: data,
            type: method,
            dataType: 'json',
            complete: function () {
                App.unblockUI(modal.find('.modal-content'));
            },
            beforeSend: function () {
                App.blockUI({
                    target: modal.find('.modal-content'),
                    animate: true,
                    overlayColor: 'none'
                });
            },
            success: function (data) {
                if (data.form_is_valid) {

                    if (typeof datatable != "undefined" && datatable != null)
                        datatable.draw(false);


                    if (typeof success_action == 'function') {
                        success_action(data);
                        return false;
                    }

                    modal.modal("hide");

                    swal({
                        title: data.title,
                        text: data.msg,
                        type: "success",
                        allowOutsideClick: true,
                        showConfirmButton: false,
                        showCancelButton: true,
                        confirmButtonClass: "",
                        cancelButtonClass: "btn-success",
                        closeOnConfirm: false,
                        closeOnCancel: true,
                        confirmButtonText: "",
                        cancelButtonText: "OK",
                    });


                } else {


                    modal.find(".modal-content").html(data.html_form);

                    toastr.error("Please correct it before submit.", "Invalid data");
                    toastr.options = {
                        "closeButton": true,
                        "debug": false,
                        "progressBar": false,
                        "positionClass": "toast-top-right",
                        "onclick": null,
                        "showDuration": "300",
                        "hideDuration": "1000",
                        "timeOut": "5000",
                        "extendedTimeOut": "1000",
                        "showEasing": "swing",
                        "hideEasing": "linear",
                        "showMethod": "fadeIn",
                        "hideMethod": "fadeOut"
                    }

                    if (typeof error_action == 'function') {
                        error_action(data);
                        return false;
                    }
                }
            },
            error: function (data) {
                showAjaxError(data, modal);
            },
        });
    };

    var saveForm = function (e) {
        e.preventDefault();

        var form = $(this);
        var url = form.attr("action");
        var data = form.serialize();
        var method = form.attr("method");
        var modal = $('#general_modal');
        var datatable = typeof window.DjangoDataTables != 'undefined' ? window.DjangoDataTables["datatable"] : null;

        AJAXSaveModal(url, data, method, modal, datatable);

        return false;
    };

    var UpdateRating = function (movie_id, user_id, rating_id, score) {

        data = {
            "user": user_id,
            "movie": movie_id,
            "score": score
        };
        API_PATCH(window.api_rating_url + rating_id + "/", data)
    };

    var CreateRating = function (movie_id, user_id) {

        data = {
            "user": user_id,
            "movie": movie_id,
            "score": 3
        };

        API_POST(window.api_rating_url, data)
    };

    var loadHTMLData = function (url, target, after_success) {

        //target = target.closest(".portlet").children(".portlet-body");
        $.ajax({
            url: url,
            type: 'GET',
            dataType: 'html',
            cache: false,
            beforeSend: function () {
                App.blockUI({
                    target: target,
                    overlayColor: '#555'
                });
            },
            complete: function () {
                App.unblockUI(target);
            },

            success: function (data) {
                target.html(data);
                if (typeof after_success != "undefined")
                    after_success();
            },
            error: function (data) {
                showAjaxError(data);
            },
        });
    };

    var loadTOP5Panel = function (url, target) {
        loadHTMLData(url, target);
    };

    return {
        removeItemAsk: removeItemAsk,
        loadForm: loadForm,
        saveForm: saveForm,
        UpdateRating: UpdateRating,
        CreateRating: CreateRating,
        loadHTMLData: loadHTMLData,
        loadTOP5Panel: loadTOP5Panel
    };

}();