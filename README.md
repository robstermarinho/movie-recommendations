### Recommendation Engine for Movies

This is a solution to deliver a web app for movies recommendation. It can suggests the top 5 movies based on the similarity between the user ratings.

Features:
  - REST API
  - Recommendation Engine proceesed by RabbitMQ
  - Django

### Installation

This app requires the following dependecies:
##### [RabbitMQ](https://www.rabbitmq.com/)
To install it on a newer Ubuntu version:

```sh
$ apt-get install -y erlang
$ apt-get install rabbitmq-server
```

Enable and start the RabbitMQ service:

```sh
$ systemctl enable rabbitmq-server
$ systemctl start rabbitmq-server
```

Check the status to make sure everything is running smooth:

```sh
$ systemctl status rabbitmq-server
```

##### [Turi Create](https://github.com/apple/turicreate/blob/master/README.md)

It's a required library to the recommendation engine algorithm.
To install on recent versions of Ubuntu, we just need a few dependencies:

```sh
$ sudo apt-get install -y libblas3 liblapack3 libstdc++6 python-setuptools libgconf-2-4
```
You can find [here](https://github.com/apple/turicreate/blob/master/LINUX_INSTALL.md) more details about Turi Create Linux installation .

#### 1. Getting Started
______

Please, make sure [Anaconda](https://www.anaconda.com/) is installed.
Also export it to the path:
```sh
$ export PATH=~/anaconda3/bin:$PATH
```

##### 1.1 Create a virtual environment and activate it

Let's create a new virtual environment `venv-movie` with conda using `python=3.5` and then activate it.

```sh
$ conda create --name venv-movie python=3.5
$ source activate venv-movie
```
Make sure the next commands will run into the activated virtual environment `venv-movie`

```sh
(venv-movie) $
```
##### 1.2 Install the requirements

Once the virtual env is activated install the project requirements:

```sh
(venv-movie) $ pip install -r requirements.txt
```

##### 1.3 Run the migrations

Run the database migrations

```sh
(venv-movie) $ python manage.py migrate
```

##### 1.4 Create the superuser

Run this command to create a superuser for authentication purposes
```sh
(venv-movie) $ python manage.py createsuperuser
```

##### 1.5 ETL Process

This is the ETL(Extract Transform Load) Process.Run this command to read the dataset files in `data/` folder, extract, transform and then populate the database.

```sh
(venv-movie) $ python manage.py populate_db
```

It takes an average of ~7 minutes (on a Dell gaming laptop):
``` sh
populate_ratings_table START...
100%|█████| 100000/100000 [06:53<00:00, 241.55it/s]
populate_ratings_table COMPLETED
```
There is an alternative option to load the data and It takes ~2 minutes (on a Dell gaming laptop). A fixture json file has been generated and can be loaded by the following command:

```sh
(venv-movie) $ python manage.py loaddata dataset_003.json
```


##### 1.6 Run Celery into the project
Open a new terminal session and run:
```sh
(venv-movie) $ celery -A atlantico worker -l info
```

##### 1.6 Run server
Open a new terminal session and run:
```sh
(venv-movie) $ python manage.py runserver
```

You can check the url [http://127.0.0.1:8000/](http://127.0.0.1:8000/) in the browser.
To go to the Django admin page you can check the url [http://127.0.0.1:8000/admin/](http://127.0.0.1:8000/admin/)
To go to the API you can check the url [http://127.0.0.1:8000/api/](http://127.0.0.1:8000/api/)


### Tests

You may run the the tests:
```sh
(venv-movie) $ python manage.py test
```