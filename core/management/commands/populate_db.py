from datetime import datetime

from django.core.management import BaseCommand
import pandas as pd
from tqdm import tqdm

from moviesapp.models import Movie, Genre
from ratingsapp.models import Rating
from usersapp.models import BasicUser, Occupation


class Command(BaseCommand):
    help = 'Populate database with the MovieLens 100K Dataset'

    def reading_genres_file(self):
        self.stdout.write(self.style.HTTP_INFO("Reading genres file..."))
        return pd.read_csv('data/ml-100k/u.genre', sep='|', names=['title', 'code'], encoding='latin-1')

    def reading_occupations_file(self):
        self.stdout.write(self.style.HTTP_INFO("Reading occupations file..."))
        return pd.read_csv('data/ml-100k/u.occupation', names=['title'], encoding='latin-1')

    def reading_users_file(self):
        self.stdout.write(self.style.HTTP_INFO("Reading users file..."))
        u_cols = ['user_id', 'age', 'sex', 'occupation', 'zip_code']
        return pd.read_csv('data/ml-100k/u.user', sep='|', names=u_cols, encoding='latin-1')

    def reading_ratings_file(self):
        self.stdout.write(self.style.HTTP_INFO("Reading ratings file..."))
        r_cols = ['user_id', 'movie_id', 'rating', 'unix_timestamp']
        return pd.read_csv('data/ml-100k/u.data', sep='\t', names=r_cols, encoding='latin-1')

    def reading_items_file(self):
        self.stdout.write(self.style.HTTP_INFO("Reading items file..."))
        i_cols = ['movie_id', 'title', 'release_date', 'video_release_date', 'imdb_url', '0', '1',
                  '2', '3', '4', '5', '6', '7', '8', '9', '10', '11', '12', '13', '14', '15', '16', '17', '18']
        return pd.read_csv('data/ml-100k/u.item', sep='|', names=i_cols,
                           encoding='latin-1')

    def populate_occupations_table(self, occupations):
        self.stdout.write(self.style.HTTP_INFO("populate_occupations_table START..."))

        self.occupations_dict = {}
        Occupation.objects.all().delete()

        with tqdm(total=len(occupations)) as pbar:
            for _, row in occupations.iterrows():
                occupation = Occupation.objects.create(title=row['title'])
                self.occupations_dict[row['title']] = occupation
                pbar.update(1)

        self.stdout.write(self.style.SUCCESS("populate_occupations_table COMPLETED"))

    def populate_basic_users_table(self, users):
        # Set a user dict with all the basic users
        self.users_dict = {}

        self.stdout.write(self.style.HTTP_INFO("populate_basic_users_table START..."))

        BasicUser.objects.all().delete()

        with tqdm(total=len(users)) as pbar:
            for index, row in users.iterrows():
                occupation = self.occupations_dict[row['occupation']]

                user = BasicUser.objects.create(age=row['age'],
                                                sex=row['sex'],
                                                occupation=occupation,
                                                zip_code=row['zip_code'],
                                                order=index)

                self.users_dict[row['user_id']] = user

                pbar.update(1)

        self.stdout.write(self.style.SUCCESS("populate_basic_users_table COMPLETED"))

    def populate_genres_table(self, genres):
        # Set a genres dict with all the genres
        self.genres_dict = {}

        self.stdout.write(self.style.HTTP_INFO("populate_genres_table START..."))

        Genre.objects.all().delete()

        with tqdm(total=len(genres)) as pbar:
            for _, row in genres.iterrows():
                genre = Genre.objects.create(title=row['title'],
                                             code=row['code'])

                self.genres_dict[genre.code] = genre
                pbar.update(1)

        self.stdout.write(self.style.SUCCESS("populate_genres_table COMPLETED"))

    def populate_movies_table(self, movies):
        self.stdout.write(self.style.HTTP_INFO("populate_movies_table START..."))

        # Set a movies dict with all the movies
        self.movies_dict = {}

        Movie.objects.all().delete()

        with tqdm(total=len(movies)) as pbar:
            for _, row in movies.iterrows():

                # Find the release date
                release_date = None
                if (row['release_date'] is not None and row['release_date'] != '' and str(
                        row['release_date']) != 'nan'):
                    release_date = datetime.strptime(str(row['release_date']), '%d-%b-%Y')

                # Find the video_release_date date
                video_release_date = None
                if (row['video_release_date'] is not None and row['video_release_date'] != '' and str(
                        row['video_release_date']) != 'nan'):
                    video_release_date = datetime.strptime(str(row['video_release_date']), '%d-%b-%Y')

                # Create the movie
                movie = Movie.objects.create(title=row['title'],
                                             release_date=release_date,
                                             video_release_date=video_release_date,
                                             imdb_url=row['imdb_url'])

                # Add the genres to the movie
                for i in range(0, 18):
                    if row[str(i)] == 1:
                        genre = self.genres_dict[i]
                        movie.genres.add(genre)

                self.movies_dict[row['movie_id']] = movie

                pbar.update(1)

        self.stdout.write(self.style.SUCCESS("populate_movies_table COMPLETED"))

    def populate_ratings_table(self, ratings):
        self.stdout.write(self.style.HTTP_INFO("populate_ratings_table START..."))

        Rating.objects.all().delete()

        with tqdm(total=len(ratings)) as pbar:
            for _, row in ratings.iterrows():
                user = self.users_dict[row['user_id']]
                movie = self.movies_dict[row['movie_id']]

                created_at = datetime.utcfromtimestamp(row['unix_timestamp']).strftime('%Y-%m-%d %H:%M:%S')
                Rating.objects.create(user=user,
                                      movie=movie,
                                      score=int(row['rating']),
                                      created_at=created_at,
                                      updated_at=created_at)
                pbar.update(1)

        self.stdout.write(self.style.SUCCESS("populate_ratings_table COMPLETED"))

    def handle(self, *args, **options):
        # Reading genres file
        genres = self.reading_genres_file()

        # Reading occupations file
        occupations = self.reading_occupations_file()

        # Reading users file:
        users = self.reading_users_file()

        # Reading ratings file:
        ratings = self.reading_ratings_file()

        # Reading items file:
        items = self.reading_items_file()

        # Populate occupations table
        self.populate_occupations_table(occupations)

        # Populate basic users table
        self.populate_basic_users_table(users)

        # Populate movies table
        self.populate_genres_table(genres)

        # Populate movies table
        self.populate_movies_table(items)

        # Populate the ratings
        self.populate_ratings_table(ratings)
