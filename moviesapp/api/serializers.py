from rest_framework.relations import HyperlinkedRelatedField, StringRelatedField
from rest_framework.serializers import ModelSerializer

from moviesapp.models import Genre, Movie


# Genre Serializer
class GenreSerializer(ModelSerializer):
    class Meta:
        model = Genre
        fields = (
            'id', 'title', 'code')


# Movie Serializer
class MovieSerializer(ModelSerializer):
    genres = StringRelatedField(many=True)

    class Meta:
        model = Movie
        fields = (
            'id', 'title', 'release_date', 'video_release_date', 'imdb_url', 'genres')

        read_only_fields = ('genres_title',)


# Movie Serializer with simple fields
class SimpleMovieSerializer(ModelSerializer):
    class Meta:
        model = Movie
        fields = (
            'id', 'title')
