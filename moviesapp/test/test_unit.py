from django.test import TestCase
from model_mommy import mommy
from moviesapp.models import Genre, Movie


# Genre Model Test
class TestGenre(TestCase):

    def setUp(self):
        self.genre = mommy.make(Genre, title='Action')

    def test_genre_creation(self):
        self.assertTrue(isinstance(self.genre, Genre))
        self.assertEqual(self.genre.__str__(), self.genre.title)


# Movie Model Test
class TestMovie(TestCase):

    def setUp(self):
        self.movie = mommy.make(Movie,
                               title='Toy Story (1995)',
                               release_date='1995-01-01',
                               imdb_url='http://us.imdb.com/M/title-exact?Toy%20Story%20(1995)')

        self.genre_a = mommy.make(Genre, title='Animation')
        self.genre_b = mommy.make(Genre, title='Comedy')

        self.movie.genres.add(self.genre_a)
        self.movie.genres.add(self.genre_b)

    def test_movie_creation(self):
        self.assertTrue(isinstance(self.movie, Movie))
        self.assertEqual(self.movie.__str__(), self.movie.title)
        self.assertEqual(self.movie.genres.count(),2)
