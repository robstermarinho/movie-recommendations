from rest_framework.serializers import ModelSerializer
from core.models import Recommendation


class RecommendationSerializer(ModelSerializer):

    class Meta:
        model = Recommendation
        fields = (
            'id', 'user', 'movie', 'score','rank', 'created_at', 'updated_at')

