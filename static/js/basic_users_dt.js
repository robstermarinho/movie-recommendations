(function (window, $) {
    window.DjangoDataTables = window.DjangoDataTables || {};
    window.DjangoDataTables["datatable"] = $("#MainDatatable").DataTable({
        "serverSide": true,
        "processing": true,
        "ajax": {
            "url": window.basic_users_dt,
            "method": "GET",
            "async": false,
        },
        "order": [[5, "desc"]],
        "columns": [
            {
                "name": "order",
                "data": 0,
                "title": "User",
                "orderable": true,
                "searchable": true
            },
            {
                "name": "sex",
                "data": 1,
                "title": "Genre",
                "orderable": true,
                "searchable": true
            },
            {
                "name": "age",
                "data": 2,
                "title": "Age",
                "orderable": true,
                "searchable": true
            },
            {
                "name": "occupation",
                "data": 3,
                "orderable": true,
                "searchable": true,
                "title": "Occupation"
            },
            {
                "name": "zip_code",
                "data": 4,
                "title": "Zip Code",
                "orderable": false,
                "searchable": false
            },
            {
                "name": "created_at",
                "data": 5,
                "orderable": true,
                "searchable": true,
                "title": "<i class='fas fa-calendar'><\/i> Created"
            },
            {
                "name": "updated_at",
                "data": 6,
                "orderable": true,
                "searchable": true,
                "title": "<i class='fas fa-clock-o'><\/i> Updated"
            },
            {
                "name": "actions",
                "data": 7,
                "title": "Actions",
                "orderable": false,
                "searchable": false
            },
        ]
    });

})(window, jQuery);

jQuery(document).ready(function () {


    $(document).on('click', '.js-remove-item', HelperComponents.removeItemAsk);
    $(document).on('click', '#add_new', HelperComponents.loadForm);
    $(document).on('submit', '#js-basic-user-form', HelperComponents.saveForm);
});
