from rest_framework.authentication import TokenAuthentication, SessionAuthentication
from rest_framework.permissions import IsAuthenticated
from rest_framework.viewsets import ViewSet
from core.api.serializers import RecommendationSerializer
from core.models import Recommendation

from django.shortcuts import get_object_or_404
from rest_framework.response import Response


class RatingsViewSet(ViewSet):
    serializer_class = RecommendationSerializer
    permission_classes = (IsAuthenticated,)
    authentication_classes = (TokenAuthentication, SessionAuthentication,)

    def list(self, request):
        queryset = Recommendation.objects.all()
        serializer = RecommendationSerializer(queryset, many=True)
        return Response(serializer.data)

    def retrieve(self, request, pk=None):
        queryset = Recommendation.objects.all()
        user = get_object_or_404(queryset, pk=pk)
        serializer = RecommendationSerializer(user)
        return Response(serializer.data)
