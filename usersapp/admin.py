from django.contrib import admin
from usersapp.models import BasicUser, Occupation

admin.site.register(Occupation)
admin.site.register(BasicUser)