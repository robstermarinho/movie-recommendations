from rest_framework.serializers import ModelSerializer

from usersapp.models import Occupation, BasicUser


# Occupation Serializer
class OccupationSerializer(ModelSerializer):
    class Meta:
        model = Occupation
        fields = ('id', 'title')


# Basic User Serializer
class BasicUserSerializer(ModelSerializer):

    class Meta:
        model = BasicUser
        fields = ('id', 'age', 'sex', 'occupation', 'zip_code', 'created_at', 'updated_at')


# BasicUser Serializer with simple fields
class SimpleBasicUserSerializer(ModelSerializer):
    class Meta:
        model = BasicUser
        fields = (
            'id', 'age', 'sex')
