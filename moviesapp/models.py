from django.db import models


class Genre(models.Model):
    '''
    This is the Genre Model.
    '''
    title = models.CharField(max_length=60)
    code = models.IntegerField()

    def __str__(self):
        return self.title


class Movie(models.Model):
    '''
    This is the Movie Model.
    '''
    title = models.CharField(max_length=200)
    release_date = models.DateField(null=True, blank=True)
    video_release_date = models.DateField(null=True, blank=True)
    imdb_url = models.CharField(max_length=200, null=True, blank=True)
    genres = models.ManyToManyField(Genre)

    def __str__(self):
        return self.title
