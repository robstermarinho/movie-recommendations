from django.db import models


class Occupation(models.Model):
    '''
    This is the Occupation Model.
    '''
    title = models.CharField(max_length=60)

    def __str__(self):
        return self.title


class BasicUser(models.Model):
    '''
    This is the Basic User Model.
    It has no any relation with the defaul django auth User Model
    '''

    # List to sex options
    SEX_OPTIONS = (
        ("M", "Male"),
        ("F", "Female"),
    )

    age = models.IntegerField()
    sex = models.CharField(choices=SEX_OPTIONS, max_length=1)
    occupation = models.ForeignKey(
        Occupation,
        on_delete=models.SET_NULL,
        related_name='basic_users',
        null=True, blank=True
    )
    zip_code = models.CharField(max_length=5)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    order = models.IntegerField(editable=False)

    # It will generate a sequential number to the basic user
    def save(self, *args, **kwargs):
        cnt = BasicUser.objects.count()
        self.order = cnt + 1
        super().save(*args, **kwargs)

    def __str__(self):
        return 'USER_' + str(self.order)
