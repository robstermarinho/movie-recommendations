from django import forms

from moviesapp.models import Movie
from ratingsapp.models import Rating
from usersapp.models import BasicUser


class RatingForm(forms.ModelForm):
    class Meta:
        model = Rating
        fields = ('user', 'movie', 'score')

class BasicUserForm(forms.ModelForm):
    class Meta:
        model = BasicUser
        fields = ('age', 'sex', 'occupation','zip_code')

class MovieForm(forms.ModelForm):
    class Meta:
        model = Movie
        fields = ('title', 'release_date', 'video_release_date','imdb_url', 'genres')