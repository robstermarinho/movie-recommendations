import time

from django.contrib.auth.mixins import LoginRequiredMixin
from django.http import JsonResponse
import requests
from django.template.loader import render_to_string
from django.views import View
from django_datatables_view.base_datatable_view import BaseDatatableView
from rest_framework.authtoken.models import Token
from django.views.generic import TemplateView
from rest_framework.reverse import reverse

from core.models import Recommendation
from dashboard.forms import RatingForm, BasicUserForm, MovieForm
from dashboard.mixins import AjaxSaveAndResponseMixin
from moviesapp.models import Movie
from ratingsapp.models import Rating
from usersapp.models import BasicUser
from .tasks import similarity_ENGINE_JOB
from dashboard.helpers import DatatableTransformations as dt_helper


# Home Template View
class DashboardHome(LoginRequiredMixin, TemplateView):
    template_name = 'dashboard/home.html'

    def dispatch(self, request, *args, **kwargs):
        # If the user is authenticated add the token to the session
        if request.user.is_authenticated and 'api_token' not in request.session:
            token, _ = Token.objects.get_or_create(user=request.user)
            request.session['api_token'] = token.key
        return super().dispatch(request, *args, **kwargs)


# Basic Users Datatable
class BasicUsersDatatable(LoginRequiredMixin, BaseDatatableView):
    model = BasicUser
    columns = ['order',
               'sex',
               'age',
               'occupation',
               'zip_code',
               'created_at',
               'updated_at',
               'actions']

    max_display_length = 500

    def render_column(self, row, column):
        if column == 'order':
            return "USER_" + str(row.order)
        elif column == 'sex':
            if row.sex == 'M':
                return '<i class="fa fa-male"></i> MALE'
            elif row.sex == 'F':
                return '<i class="fa fa-female"></i> FEMALE'
        elif column == 'occupation':
            return dt_helper.highlightBlue(row.occupation.title.upper())
        elif column == 'created_at':
            return dt_helper.generateDateLabel(row.created_at)
        elif column == 'updated_at':
            return dt_helper.generateNaturalTime(row.updated_at, 'bg-red-flamingo bg-font-red-flamingo')
        elif column == 'actions':
            return render_to_string('basicusers/partial_users_dt.html', {"user": row, 'request': self.request})
        else:
            return super(BasicUsersDatatable, self).render_column(row, column)


# Basic if the user lost the session
class SessionLostCheck(View):

    def dispatch(self, request, *args, **kwargs):
        data = {
            'status': request.user.is_authenticated
        }
        return JsonResponse(data, status=200)


# Basic Users Form Creation
class BasicUserFormView(LoginRequiredMixin, AjaxSaveAndResponseMixin, View):
    template_name = 'basicusers/basic_users_form.html'
    modal_title = 'New Evaluator'
    model = BasicUser
    form_class = BasicUserForm
    form_fields = ['id', 'age', 'sex', 'zip_code']


class MoviesFormView(LoginRequiredMixin, AjaxSaveAndResponseMixin, View):
    template_name = 'movies/movie_form.html'
    modal_title = 'New Movie'
    model = Movie
    form_class = MovieForm
    form_fields = ['id', 'title', 'release_date', 'video_release_date', 'imdb_url']


# User Ratings Datatable
class UserRatingsDatatable(LoginRequiredMixin, BaseDatatableView):
    model = Movie
    columns = ['title',
               'release_date',
               'imdb_url',
               'genres',
               'rating'
               ]

    max_display_length = 500

    def dispatch(self, request, *args, **kwargs):
        self.basic_user = BasicUser.objects.get(id=kwargs['pk'])
        return super().dispatch(request, *args, **kwargs)

    def render_column(self, movie, column):

        if column == 'genres':
            html = ''
            for genre in movie.genres.all():
                html += '<span class="label label-default">' + genre.title + '</span>'
            return html
        elif column == 'release_date':
            if movie.release_date:
                return dt_helper.generateDateLabel(movie.release_date)
        elif column == 'rating':
            rating = Rating.objects.filter(movie=movie, user=self.basic_user)
            if rating.count():
                rating = rating.first()
            else:
                rating = None

            return render_to_string('ratings/partial_rating_stars.html', {"rating": rating,
                                                                          'movie': movie,
                                                                          'user': self.basic_user})
        elif column == 'imdb_url':
            return '<a target="_blank" href="' + movie.imdb_url + '"><button class="btn btn-primary btn-xs">IMDB URL <i class="fa fa-external-link-alt"></i></button></a>'
        else:
            return super(UserRatingsDatatable, self).render_column(movie, column)


# Movies Datatable
class MoviesDatatable(LoginRequiredMixin, BaseDatatableView):
    model = Movie
    columns = ['title',
               'release_date',
               'imdb_url',
               'genres',
               'ratings'
               ]

    max_display_length = 500

    def render_column(self, movie, column):

        if column == 'genres':
            html = ''
            for genre in movie.genres.all():
                html += '<span class="label label-default">' + genre.title + '</span>'
            return html
        elif column == 'release_date':
            if movie.release_date:
                return dt_helper.generateDateLabel(movie.release_date)
        elif column == 'ratings':
            return int(movie.ratings.count())
        elif column == 'imdb_url':
            return '<a target="_blank" href="' + movie.imdb_url + '"><button class="btn btn-primary btn-xs">IMDB URL <i class="fa fa-external-link-alt"></i></button></a>'
        else:
            return super(MoviesDatatable, self).render_column(movie, column)


# User Ratings Page
class UserRatingsView(LoginRequiredMixin, TemplateView):
    template_name = 'ratings/ratings.html'

    def dispatch(self, request, *args, **kwargs):
        self.basic_user = BasicUser.objects.get(id=kwargs['pk'])
        return super().dispatch(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['basic_user'] = self.basic_user
        return context


class UserTop5MoviesView(LoginRequiredMixin, TemplateView):
    template_name = 'basicusers/top5_movies.html'

    def dispatch(self, request, *args, **kwargs):
        self.basic_user = BasicUser.objects.get(id=kwargs['pk'])
        return super().dispatch(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['basic_user'] = self.basic_user
        context['top5'] = self.basic_user.recommendations.all().order_by('rank')
        return context


class MoviesView(LoginRequiredMixin, TemplateView):
    template_name = 'movies/movies.html'
