from django.contrib import admin

from ratingsapp.models import Rating

admin.site.register(Rating)