from django.db import models

from moviesapp.models import Movie
from ratingsapp.validators import validate_rating_score
from usersapp.models import BasicUser


class Rating(models.Model):
    '''
    This is the Rating Model.
    It store the ratings for different user and movie combination
    '''
    user = models.ForeignKey(
        BasicUser,
        on_delete=models.CASCADE,
        related_name='ratings',
    )
    movie = models.ForeignKey(
        Movie,
        on_delete=models.CASCADE,
        related_name='ratings',
    )
    score = models.SmallIntegerField(validators=[validate_rating_score])
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    class Meta:
        unique_together = (("user", "movie"),)

    def __str__(self):
        return str(self.user) + " | " + self.movie.title + " | " +  str(self.score)