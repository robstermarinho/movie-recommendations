from django.test import TestCase
from model_mommy import mommy
from usersapp.models import Occupation, BasicUser


# Occupation Model Test
class TestOccupation(TestCase):

    def setUp(self):
        self.occupation = mommy.make(Occupation, title='writer')

    def test_occupation_creation(self):
        self.assertTrue(isinstance(self.occupation, Occupation))
        self.assertEqual(self.occupation.__str__(), self.occupation.title)


# BasicUser Model Test
class TestBasicUser(TestCase):

    def setUp(self):
        self.user = mommy.make(BasicUser,
                                     age=24,
                                     sex='M',
                                     zip_code='85711')



    def test_user_creation(self):
        self.assertTrue(isinstance(self.user, BasicUser))
        self.assertEqual(self.user.__str__(), 'USER_' + str(self.user.id))


        # Test if the ORDER number is being decreased
        self.assertTrue(BasicUser.objects.count(), 3)


    def test_ORDER_number_increase(self):
        # Create 14 Random Users (14 + 1 = 15)
        for i in range(14):
            self.user = mommy.make(BasicUser)

        # Test if the ORDER number is being increased
        self.assertTrue(BasicUser.objects.count(), 15)

    def test_ORDER_number_decrease(self):

        # Create 14 Random Users (14 + 1 = 15)
        for i in range(14):
            self.user = mommy.make(BasicUser)

        # Delete 6 users (15 - 6 = 9)
        for i in range(6):
            user = BasicUser.objects.first()

        # Test if the ORDER number is being decrease
        self.assertTrue(BasicUser.objects.count(), 9)