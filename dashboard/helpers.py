from django.contrib.humanize.templatetags.humanize import naturaltime
from django.utils.html import escape



# Datatable helper Transformations
class DatatableTransformations():

    def highlightBlue(text):
        return "<strong class='font-blue-soft'>" + str(text) + "</strong>"

    def highlightOrange(text):
        return "<strong class='font-yellow-casablanca'>" + str(text) + "</strong>"

    def generateHeaderCheckBox(table_id):
        return '<label class="mt-checkbox mt-checkbox-single mt-checkbox-outline"><input type="checkbox" class="group-checkable" data-set="' + str(
            table_id) + ' .checkboxes" /><span></span></label>'

    def generateRowCheckBox(item_id):
        return '<label class="mt-checkbox mt-checkbox-single mt-checkbox-outline"><input type="checkbox" class="checkboxes" value="' + str(
            item_id) + '" /><span></span></label>'

    def generateNaturalTime(date, bg_color='bg-blue-madison bg-font-blue-madison'):
        return '<span style="font-size:13px;" class="label ' + str(
            bg_color) + '"></i> ' + naturaltime(date) + '</span>'

    def generateDateLabel(date, bg_color='bg-blue-madison bg-font-blue-madison'):
        return '<span style="font-size:13px;" class="label ' + str(
            bg_color) + '"> ' + escape("{:%m/%d/%Y}".format(date)) + '</span>'

    def generateDateHourLabel(date, display_hour=True, bg_color='bg-blue-madison bg-font-blue-madison'):
        dt = '<span style="font-size:13px;" class="label ' + str(
            bg_color) + '"> ' + escape("{:%m/%d/%Y}".format(date)) + '</span>'

        hour = '<span style="font-size:13px;" class="label bg-yellow-casablanca"><i class="fa fa-clock-o"></i> ' + escape(
            "{:%H:%M:%S}".format(date)) + '</span>'

        if display_hour:
            return dt + hour
        return dt

    def generateLink(url):
        if url and url != '':
            return '<a target="_blank" href="' + url + '">' + url + '</a>'
