import os
from celery import Celery

os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'atlantico.settings')

# Celery Configuration
app = Celery('atlantico')
app.config_from_object('django.conf:settings', namespace='CELERY')
app.autodiscover_tasks()
