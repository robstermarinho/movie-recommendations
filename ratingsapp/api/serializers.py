from rest_framework.serializers import ModelSerializer
from dashboard.tasks import similarity_ENGINE_JOB
from moviesapp.api.serializers import SimpleMovieSerializer
from ratingsapp.models import Rating

# Rating Serializer
from usersapp.api.serializers import SimpleBasicUserSerializer


class RatingSerializer(ModelSerializer):
    # movie = SimpleMovieSerializer()
    # user = SimpleBasicUserSerializer()

    class Meta:
        model = Rating
        fields = (
            'id', 'user', 'movie', 'score', 'created_at', 'updated_at')


    def create(self, validated_data):
        rating = Rating.objects.create(**validated_data)
        rating.save()
        similarity_ENGINE_JOB.delay(rating.user.id)
        return rating

    def partial_update(self, request, *args, **kwargs):
        user = args[0]['user']
        partial_update = super(RatingSerializer, self).partial_update(request, *args, **kwargs)
        similarity_ENGINE_JOB.delay(user.id)
        return partial_update

    def update(self, request, *args, **kwargs):
        user = args[0]['user']
        update = super(RatingSerializer, self).update(request, *args, **kwargs)
        similarity_ENGINE_JOB.delay(user.id)
        return update
