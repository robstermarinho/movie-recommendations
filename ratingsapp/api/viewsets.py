from rest_framework.authentication import TokenAuthentication, SessionAuthentication
from rest_framework.permissions import IsAuthenticated
from rest_framework.viewsets import ModelViewSet
from ratingsapp.api.serializers import RatingSerializer
from ratingsapp.models import Rating


class RatingsViewSet(ModelViewSet):
    serializer_class = RatingSerializer
    permission_classes = (IsAuthenticated,)
    authentication_classes = (TokenAuthentication, SessionAuthentication,)

    # get the ratings list ordered by created_at desc
    def get_queryset(self):
        return Rating.objects.order_by('-created_at').all()