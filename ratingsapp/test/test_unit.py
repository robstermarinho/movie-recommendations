from django.test import TestCase
from model_mommy import mommy

from moviesapp.models import Movie
from ratingsapp.models import Rating

# Rating Model Test
from usersapp.models import BasicUser


class TestRating(TestCase):

    def setUp(self):
        self.movie = mommy.make(Movie,
                                title='Toy Story (1995)',
                                release_date='1995-01-01',
                                imdb_url='http://us.imdb.com/M/title-exact?Toy%20Story%20(1995)')

        self.user = mommy.make(BasicUser,
                               age=24,
                               sex='M',
                               zip_code='85711')

        self.rating = mommy.make(Rating,
                                 movie=self.movie,
                                 user=self.user,
                                 score=4)

    def test_genre_creation(self):
        self.assertTrue(isinstance(self.rating, Rating))
        self.assertEqual(self.rating.score, 4)
