# Ajax Save And Response Mixin
# This class implements the behavior of saving a modal form instance
# And send some data to JSON response object
# The following attributes should be defined in the class based view:
#
# template_name = ''                # Form html template
# modal_title = ''                  # Modal title that should be displayed on the modal html
# model =                           # Model Class of the object
# form_class = EventForm            # Form Class you want to implement
# fields = []                       # Form fields you want to sent to the JSON response after save the object instance
#
from django.http import JsonResponse
from django.shortcuts import get_object_or_404
from django.template.loader import render_to_string


class AjaxSaveAndResponseMixin:

    # Send AJAX response when the User has no permission
    def handle_no_permission(self):
        data = {}
        data['title'] = 'Request Fail'
        data['msg'] = 'You have no sufficient permissions <br>'
        data['msg'] += '<small class="font-red-flamingo">' + str(self.model.__name__) + ' - ' + str(
            self.permission_required) + '</small>'
        return JsonResponse(data, status=403)

    # After submit some POST/GET form save it and return Json response
    def saveAjaxForm(self, form, object):
        data = {}
        status = 200

        if self.request.is_ajax() and self.request.method == 'POST':
            if form.is_valid():
                object_instance = self.saveFormLogic(form)
                data['form_is_valid'] = True
                data['title'] = 'Success'
                data['msg'] = 'The ' + str(self.model.__name__) + ' has been saved!'

                if hasattr(self, 'form_fields'):
                    form_fields = self.form_fields
                else:
                    form_fields = None
                if form_fields is not None and len(form_fields) > 0:
                    data['object'] = {}
                    for field in form_fields:
                        data['object'][field] = getattr(object_instance, field)

            else:
                data['form_is_valid'] = False
                data['form_errors'] = form.errors
                data['title'] = 'Request Fail'
                data['msg'] = 'The ' + str(self.model.__name__) + ' cannot be saved!'
                # status = 400

        context = {
            'form': form,
            'modal_title': self.modal_title,
            'request_path': self.request.path,
            'object': object
        }

        data['html_form'] = render_to_string(self.template_name, context, request=self.request)
        return JsonResponse(data, status=status)

    # Save Form Logic
    def saveFormLogic(self, form):
        return form.save()

    # Instantiate the right form and modal title based on the request
    def actionForm(self, pk, request):
        object = None
        form = None
        if pk is not None:  # If it´s an update
            object = get_object_or_404(self.model, pk=pk)  # Find the object by 'pk'
            if self.modal_title == '':
                self.modal_title += 'Update '  # Change the modal title to the group name
                self.modal_title += str(self.model.__name__)
                self.modal_title += ' | ' + str(object)
            if self.request.method == 'GET':
                form = self.form_class(instance=object)  # Instantiate the form
            elif self.request.method == 'POST':
                form = self.form_class(data=request.POST, files=request.FILES, instance=object)  # Instantiate the form

        else:  # If it´s a creation
            if self.modal_title == '':
                self.modal_title += 'Create '  # Change the modal title
                self.modal_title += str(self.model.__name__)

            if self.request.method == 'GET':
                form = self.form_class()  # Instantiate the form
            elif self.request.method == 'POST':
                form = self.form_class(data=request.POST, files=request.FILES)

        return self.saveAjaxForm(form, object)  # Save the form and send the AJAX response

    def get(self, request, pk=None, *args, **kwargs):  # Triggered after GET method
        return self.actionForm(pk, request)

    def post(self, request, pk=None, *args, **kwargs):  # Triggered after POST method
        return self.actionForm(pk, request)
