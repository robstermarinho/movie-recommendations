from django.db import models

from moviesapp.models import Movie
from usersapp.models import BasicUser


class Recommendation(models.Model):
    '''
    This is the Recommendation Model.
    It stores the movie recommendations to the basic users
    '''
    user = models.ForeignKey(
        BasicUser,
        on_delete=models.CASCADE,
        related_name='recommendations',
    )
    movie = models.ForeignKey(
        Movie,
        on_delete=models.CASCADE,
        related_name='recommendations',
    )
    score = models.DecimalField(max_digits=22, decimal_places=20)
    rank = models.SmallIntegerField()

    class Meta:
        unique_together = (("user", "movie"),)

    def __str__(self):
        return str(self.user) + " | " + self.movie.title + " | " + str(self.score) + " | " + str(self.rank)