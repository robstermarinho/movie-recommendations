    (function (window, $) {
        window.DjangoDataTables = window.DjangoDataTables || {};
        window.DjangoDataTables["datatable"] = $("#MainDatatable").DataTable({
            "serverSide": true,
            "processing": true,
            "ajax": {
                "url": window.user_ratings_dt,
                "method": "GET",
                "async": false,
            },
            "order": [[1, "desc"]],
            "columns": [
                {
                    "name": "title",
                    "data": 0,
                    "title": "<i class='fas fa-film'><\/i> Movie Title",
                    "orderable": true,
                    "searchable": true
                },
                {
                    "name": "release_date",
                    "data": 1,
                    "title": "Release Date",
                    "orderable": true,
                    "searchable": true
                },
                {
                    "name": "imdb_url",
                    "data": 2,
                    "orderable": false,
                    "searchable": false,
                    "title": "IMDb URl"
                },
                {
                    "name": "genres",
                    "data": 3,
                    "title": "Genres",
                    "orderable": false,
                    "searchable": false
                },
                {
                    "name": "rating",
                    "data": 4,
                    "orderable": false,
                    "searchable": false,
                    "title": "<i class='fas fa-star'><\/i> Rating"
                },
            ],
            "createdRow": function (row, data, dataIndex) {

                $(row).find('.movie_rate').barrating({
                    theme: 'fontawesome-stars',
                    showSelectedRating: false,
                    allowEmpty: false,
                    onSelect: function (value, text, event) {
                        var select = $(this);
                        select = $(select[0].$elem[0]);

                        var movie_id = select.data('movie_id');
                        var user_id = select.data('user_id');
                        var rating_id = select.data('rating_id');


                        HelperComponents.UpdateRating(movie_id, user_id, rating_id, value);
                    }
                });

            }
        });

    })(window, jQuery);

    jQuery(document).ready(function () {


        $(document).on('click', '.js-rate_movie', function () {
            var movie_id = $(this).data('movie_id');
            var user_id = $(this).data('user_id');

            HelperComponents.CreateRating(movie_id, user_id);
        });

        // Load Panel
        HelperComponents.loadTOP5Panel(window.user_top5_url, $("#top_5_user_panel"));

        // Reload Panel every 5 seconds


        setInterval(function () {
         //   $(".top5_panel_reload").click();
            HelperComponents.loadTOP5Panel(window.user_top5_url, $("#top_5_user_panel"));
        }, 5000);


        $(document).on('click', '.reload_top_5_btn', function (e) {
            e.preventDefault();
            HelperComponents.loadTOP5Panel(window.user_top5_url, $("#top_5_user_panel"));
        })
    });