from django.core.exceptions import ValidationError

def validate_rating_score(score):
    if not 1 <= score <= 5:
        raise ValidationError(u'The score must be a value between 1 and 5.')
    return score
